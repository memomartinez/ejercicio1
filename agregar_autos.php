<!doctype html>
<html lang="es">
  <head>    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" />
    <title>agregar autos</title>
  </head>
  <body>
    <br/>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h4>Agregar Autos</h4>
            </div>
            <div class="card-body">
                <form action = "" method = "POST">
                    <label>nombre:</label>
                    <input type = "text" name = "nombreAuto" id = "nombreAuto" class="form-control" />
                    <br />
                    <label>modelo:</label>
                    <input type = "text" name = "modeloAuto" id = "modeloAuto" class="form-control"/>
                    <br />
                    <label>TipoGasolina:</label>
                    <input type = "text" name = "gasolinaAuto" id = "gasolinaAuto" class="form-control"/>
                    <br />
                    <label>CantidadPuertas</label>:
                    <input type = "text" name = "cantidadPuertas" id = "cantidadPuertas" class="form-control"/>
                    <br />
                    <input type = "Submit" value ="Guardar" name = "submit" class="btn btn-success"/>
                    <a class="btn btn-info" href="listar_carros.php">Inicio</a>
                    <br />
                </form>
            </div>
        </div>
    </div>

    <?php
         if(isset($_POST["submit"])){
            include 'conexion.php';
            $conn = OpenCon();
           
            // Verificamos la conexión
            if ($conn->connect_error) {
               die("No se pudo conectar a la base de datos: " . $conn->connect_error);
            } 
            $sql = "INSERT INTO autos(, nombreAuto, modeloAuto, gasolinaAuto, cantidadPuertas)VALUES ('".$_POST["nombreAuto"]."','".$_POST["modeloaAuto"]."','".$_POST["gasolinaAuto"]."','".$_POST["cantidadPuestas"]."')";

            if (mysqli_query($conn, $sql)) {
               echo "<div class=\"alert alert-success\" role=\"alert\">";
               echo "Se ha guardado el autor";
               echo "</div>";
            } else {
               echo "<div class=\"alert alert-danger\" role=\"alert\">";
               echo "No se pudo guardar el autor. ";
               echo "Error: " . $sql . "" . mysqli_error($conn);
               echo "</div>";               
            }
            $conn->close();
         }
      ?>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>

